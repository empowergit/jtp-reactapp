export function UserRoles() {
    let BaseURL = 'http://localhost:8080/reactapi/get_user_roles.php';
    return new Promise((resolve, reject) =>{
        fetch(BaseURL, {
            method: 'GET'
          })
          .then((response) => response.json())
          .then((res) => {
            resolve(res);
          })
          .catch((error) => {
            reject(error);
          });
      });
}