export function GetUserPermissionsByRoleID(userData) {
    let BaseURL = 'http://localhost:8080/reactapi/get_user_permission_by_role_id.php';
    return new Promise((resolve, reject) =>{
        fetch(BaseURL, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: JSON.stringify(userData)
          })
          .then((response) => response.json())
          .then((res) => {
            resolve(res);
          })
          .catch((error) => {
            reject(error);
          });
      });
}