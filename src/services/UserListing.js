export function UserListing(type, userData) {
    let BaseURL = 'http://localhost:8080/reactapi/userList.php';
    return new Promise((resolve, reject) =>{
        fetch(BaseURL, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
          })
          .then((response) => response.json())
          .then((res) => {
            resolve(res);
          })
          .catch((error) => {
            reject(error);
          });
      });
}