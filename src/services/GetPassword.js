export function GetPassword(type, userData) {
    let BaseURL = 'http://localhost:8080/reactapi/forgot_password.php';

    return new Promise((resolve, reject) =>{


        fetch(BaseURL, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: JSON.stringify(userData)
          })
          .then((response) => response.json())
          .then((res) => {
            resolve(res);
          })
          .catch((error) => {
            reject(error);
          });


      });
}