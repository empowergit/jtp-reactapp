export function GetSections() {
    let BaseURL = 'http://localhost:8080/reactapi/get_sections.php';
    return new Promise((resolve, reject) =>{
        fetch(BaseURL, {
            method: 'GET'
        })
            .then((response) => response.json())
            .then((res) => {
                resolve(res);
            })
            .catch((error) => {
                reject(error);
            });
    });
}