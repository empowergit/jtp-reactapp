import React, {Component} from 'react';
import {checkLogin,checkPermission} from '../../Utils';
import {Redirect} from "react-router-dom";
import {UserRoles} from "../../services/UserRoles";
import {GetSectionsByRoleID} from "../../services/GetSectionsByRoleID";
import {SetUserPermissionsByRoleID} from "../../services/SetUserPermissionsByRoleID";
class Permissions extends Component {
    constructor() {
        super();
        this.state = {
            userRoleList:[],
            sectionsList:[],
            role_id:"1",
            section_id:[],
            success:"",
            message:""
        }
        this.onChange = this.onChange.bind(this);
    }
     componentDidMount() {
        UserRoles().then((result) => {
            let responseJson = result;
            if (responseJson.results) {
                var obj=responseJson.results;
                if(obj.statusCode=="1") {
                    this.setState({userRoleList: obj.result});
                }else{
                    this.setState({userRoleList: ""});
                }
            }
        });

         let objSection = {
             role_id: this.state.role_id
         };

         GetSectionsByRoleID(objSection).then((result) => {
            let responseJson = result;
            if (responseJson.results) {
                var obj=responseJson.results;
                if(obj.statusCode=="1") {
                    this.setState({sectionsList: obj.result});
                }else{
                    this.setState({sectionsList: ""});
                }
            }
        });

    }
    onChange(event){
        const isCheckbox = event.target.type === "checkbox";
        this.setState({
            [event.target.name]: isCheckbox
                ? event.target.checked
                : event.target.value
        });
        let e = document.getElementById("role_id");
        let role_id = e.options[e.selectedIndex].value;
        let objSectionChange = {
            role_id: role_id
        };

        GetSectionsByRoleID(objSectionChange).then((result) => {
            let responseJson = result;
            if (responseJson.results) {
                var obj=responseJson.results;
                if(obj.statusCode=="1") {
                    this.setState({sectionsList: obj.result});
                }else{
                    this.setState({sectionsList: ""});
                }
            }
        });
    }
    onToggle(index, e){
        let newItems = this.state.sectionsList.slice();
        newItems[index].checked = !newItems[index].checked

        this.setState({
            section_id: newItems
        })
    }
    handleSubmit = event => {
        event.preventDefault();
        let sections = this.state.sectionsList.filter(item => item.checked);
        let obj = {
            role_id: this.state.role_id,
            sections
        };
        SetUserPermissionsByRoleID(obj).then((result) => {
            let responseJson = result;
            if (responseJson.results) {
                var obj=responseJson.results;
                if(obj.statusCode=="1") {
                    this.setState({success: "1",message:obj.message});
                }else{
                    this.setState({success: "0",message:obj.message});
                }
            }
        });
    }
    render() {
        if(checkLogin()==false){
            return (<Redirect to={'/login'}/>)
        }
        if(checkPermission()==false){
            return <div className="text-center">
                <h1 className="permission-error">You don't have permission to see this section. Contact to administrator.</h1>
            </div>
        }else {
            const userRoleList = this.state.userRoleList;
            let user_roles = userRoleList.map((data) =>
                <option
                    key={data.role_id}
                    value={data.role_id}
                >
                    {data.role_name}
                </option>
            );
            const sectionsList = this.state.sectionsList;
            let sections = sectionsList.map((data,i) =>
                <React.Fragment>
                    <li key={i}><input type="checkbox" checked={data.checked} name="section_id" value={data.section_id} onChange={this.onToggle.bind(this, i)}/><span>{data.section_name}</span></li>
                </React.Fragment>
            );

            return (
                <div>
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6">
                                <h4>Permissions</h4>
                                { this.state.success == "1"  && <p className="alert alert-success">{this.state.message}</p>}
                                { this.state.success == "0" && <p className="alert alert-danger">{this.state.message}</p>}
                            </div>
                        </div>
                        <form onSubmit={this.handleSubmit}>
                        <div className="row">
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>User Roles</label>
                                    <select name="role_id" id="role_id" className="form-control" onChange={this.onChange}>
                                        {user_roles}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>Section</label>
                                        <ul className="permission_list">
                                            {sections}
                                        </ul>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6">
                                <div className="form-group">
                                    <button type="submit" className="btn btn-warning success">
                                        Submit <i className="fa fa-check-circle" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            );
        }
    }
}

export default Permissions;