import React from "react";
import ProductList from './ProductList';
import loader from "../../img/loader.gif";
export default class ProductListing extends React.Component {
    state = {
        loading: true,
        products: null,
        cartItems: false
    };

    async componentDidMount() {
        const url = "http://localhost:8080/reactapi/product.php";
        const response = await fetch(url,{headers: { 'Content-Type': 'application/x-www-form-urlencoded' }});
        const data = await response.json();
        this.setState({ products: data.results, loading: false });
    }

    render() {
        if (this.state.loading) {
            return <div > <img src={loader} className="loader"/> < /div>;
        }

        if (!this.state.products) {
            return <div > didn 't get a person</div>;
        }

        const arrayproductlist = this.state.products.map((productlist, i) => {
            return <ProductList key = { this.state.products[i].id }
            id = { this.state.products[i].id }
            name = { this.state.products[i].name }
            price = { this.state.products[i].price }
            image = { this.state.products[i].image }
            />
        })

        return ( 
            <div className = "text-center" >
                    <div className = "container" >
                        <div className = "row" > { arrayproductlist } </div> 
                    </div> 
            </div>
        );
    }
}