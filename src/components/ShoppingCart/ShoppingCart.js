import React from "react";
import Cart from './Cart';
import { Route, Link, BrowserRouter as Router } from 'react-router-dom';
export default class ShoppingCart extends React.Component {
    state = {
        cartItems: false
    };

    async componentDidMount() {
        var cartData = sessionStorage.getItem('cartData');
        if (cartData != null) { this.setState({ cartItems: true }); }
    }
    render() {
        if (this.state.cartItems) {
            var cartData = sessionStorage.getItem('cartData');
            var arraycartlist = [];
            var final_cart = JSON.parse(cartData);

            arraycartlist = final_cart.map((cartlist, i) => {
                return <Cart key = { final_cart[i].id }
                             id = { final_cart[i].id }
                             name = { final_cart[i].name }
                             price = { final_cart[i].price }
                />
            });

            var totalPrice = sessionStorage.getItem('totalPrice');

        }

        return ( 
            <div className = "text-center" >
                <div className = "container" >
                    <div className = "row mt-5" >
                        <div className = "col-md-12" >
                            <div className = "continue-shopping" >
                                <Link to = "/ProductListing" className = "btn btn-warning" > Continue Shopping <i
                                    className="fa fa-shopping-cart" aria-hidden="true"></i>
                                    < /Link>
                            </div>
                            <table border = "1" className = "w-100" >
                            <thead>
                            <td> Product </td> 
                            <td> Price </td> 
                            <td> Action </td> 
                            </thead>
                            <tbody id = "cart_items"> { arraycartlist } </tbody> 
                            </table> 
                            <p><strong> Total </strong>: <span id="total_price">${totalPrice}</span></p> 
                        </div> 
                    </div> 
                </div> 
            </div>
        );
    }


}