import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';


class Productlist extends Component {
  constructor() {
      super();
      this.state = {
          redirectToReferrer: false
      };
      this.addToCart = this.addToCart.bind(this);
  }
  addToCart() {
      console.log(this.props);
      var obj=this.props;
      var cartData=sessionStorage.getItem('cartData');
      var totalPrice=sessionStorage.getItem('totalPrice');
      var updatedCartData=[];
      if((cartData == null) || (cartData == '[]')){
          updatedCartData.push(obj);
          sessionStorage.setItem('cartData', JSON.stringify(updatedCartData));
          sessionStorage.setItem('totalPrice', obj.price);
      }else{
          var initial=JSON.parse(cartData);
          var hasMatch ='0';
          for (var index = 0; index < initial.length; ++index) {
              var chk = initial[index];
              if(chk.id == obj.id){
                  hasMatch = '1';
              }
          }
          if(hasMatch=='0'){
              updatedCartData.push(initial[0]);
              updatedCartData.push(obj);
              sessionStorage.setItem('cartData', JSON.stringify(updatedCartData));
              var finalPrice= parseInt(obj.price) + parseInt(totalPrice);
              sessionStorage.setItem('totalPrice', finalPrice);
          }
      }
      this.setState({redirectToReferrer: true});
  }
  render() {
      if (this.state.redirectToReferrer) {
          return (<Redirect to={'/ShoppingCart'}/>)
      }
      return <div className="product-bx col-md-2">
          <img src={this.props.image} />
          <div>{this.props.name}</div>
          <div>${this.props.price}</div>
          <button onClick={this.addToCart.bind()} className="btn btn-warning" type="submit">Add to Cart <i
              className="fa fa-shopping-cart" aria-hidden="true"></i>
          </button>
      </div>
  }
}
export default Productlist;