import React, { useState } from 'react';

const Cart = (props) => {
    var rowID="cartList_"+props.id;
    function removeFromCart(id) {
        var updatedCartData=[];
        var cartData=sessionStorage.getItem('cartData');
        var final_cart=JSON.parse(cartData);
        updatedCartData.push(final_cart);
        for( var i = 0; i < updatedCartData[0].length; ++i ) {
            if( updatedCartData[0][i]['id'] == id ) {
                updatedCartData[0].splice( i, 1 );
            }
        }
        var finalPrice=parseInt('0');
        for( var j = 0; j < updatedCartData[0].length; ++j ) {
            finalPrice += parseInt(updatedCartData[0][j]['price']);
        }
        sessionStorage.setItem('totalPrice', finalPrice);
        sessionStorage.setItem('cartData', JSON.stringify(updatedCartData[0]));
        document.getElementById("cartList_"+id).style.opacity="0";
        document.getElementById("cartList_"+id).style.transition="opacity 1000ms";

        setTimeout(function(){ document.getElementById("cartList_"+id).style.display="none"; }, 500);
        document.getElementById("total_price").innerHTML="$ "+sessionStorage.getItem('totalPrice');
    }
    return <React.Fragment>
        <tr id={ rowID }>
        <td>{props.name}</td>
        <td>${props.price}</td>
            <td><a  href="javascript:void(0);" onClick={removeFromCart.bind(this,props.id)} className="badge badge-danger remove-product"><i
                className="fa fa-times" aria-hidden="true"></i>
            </a></td>
        </tr>
        </React.Fragment>

}
export default Cart;