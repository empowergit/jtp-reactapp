import React from 'react';
const Avatarlist = (props) => {
  return <div className="avatarstyle col-md-4">
  <img src={`https://joeschmoe.io/api/v1/${props.name}`} /> 
  <h1>{props.name}</h1>
  <p>{props.work}</p>
  </div>
 
}
 export default Avatarlist;