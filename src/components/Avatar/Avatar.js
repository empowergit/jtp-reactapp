import React from 'react';
import {Redirect} from "react-router-dom";
import {checkLogin,checkPermission} from '../../Utils';
import Avatarlist from './Avatarlist';
 const Avatar = () => {
     if(checkLogin()==false){
         return (<Redirect to={'/login'}/>)
     }
   const avatararrylist = [
     {
        id:1,
        name:"nick",
        work:"webdeveloper"
     },
     {
        id:2,
        name:"john",
        work:"webdeveloper"
     },
     {
        id:3,
        name:"jordan",
        work:"webdeveloper"
     }
   ]
  
   const arrayavatarcard = avatararrylist.map((avatarcard,i) => {
   
      return <Avatarlist key={avatararrylist[i].id}
                         name={avatararrylist[i].name}
                         work={avatararrylist[i].work} />

   }

  )

     if(checkPermission()==false){
         return <div className="text-center">
             <h1 className="permission-error">You don't have permission to see this section. Contact to administrator.</h1>
         </div>
     }else{
         return <div className="text-center">
             <h1>Welcome to the world of avatar</h1>
             <div className="container">
                 <div className="row">
                     {arrayavatarcard}
                 </div>
             </div>
         </div>
     }
 }

 export default Avatar;