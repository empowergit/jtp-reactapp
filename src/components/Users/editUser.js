import React, {Component} from 'react';
import {checkLogin,checkPermission} from '../../Utils';
import {GetUserByUserID} from "../../services/GetUserByUserID";
import {UpdateUser} from "../../services/UpdateUser";
import loader from "../../img/loader.gif";
import {Redirect} from "react-router-dom";
const initialState = {
    user_id:"",
    role_id:"",
    name: "",
    email: "",
    status: "",
    nameError: "",
    emailError: "",
    success:"",
    message:""
};
class EditUser extends Component {
    state = initialState;

    componentDidMount() {
        var user_id={"user_id":this.props.match.params.user_id};
        GetUserByUserID(user_id).then((result) => {
            let responseJson = result;
            if (responseJson.results) {
                var obj=responseJson.results;
                if(obj.statusCode=="1") {
                    this.setState({user_id:obj.result["user_id"],role_id:obj.result["role_id"],name: obj.result['name'],email:obj.result['email'],status:obj.result["status"]});
                }
            }
        });
    }

    handleChange = event => {
        const isCheckbox = event.target.type === "checkbox";
        this.setState({
            [event.target.name]: isCheckbox
                ? event.target.checked
                : event.target.value
        });
    };

    validate = () => {
        let nameError = "";
        let emailError = "";

        if (!this.state.name) {
            nameError = "Name cannot be blank";
        }

        if (!this.state.email.includes("@")) {
            emailError = "invalid email";
        }



        if (emailError || nameError) {
            this.setState({ emailError, nameError });
            return false;
        }

        return true;
    };
    handleSubmit = event => {
        event.preventDefault();
        const isValid = this.validate();
        if (isValid) {
            let loader=document.getElementById("loader-user");
            loader.style.display="block";
            UpdateUser( this.state).then((result) => {
                let responseJson = result;
                if (responseJson.results) {
                    loader.style.display="none";
                    var obj=responseJson.results;
                    if(obj.statusCode=="1") {
                        this.setState({success: "1",message:obj.message});
                    }else{
                        this.setState({success: "0",message:obj.message});
                    }
                }
            });
        }
    }
    render() {
        if(checkLogin()==false){
            return (<Redirect to={'/login'}/>)
        }
        if(checkPermission()==false){
            return <div className="text-center">
                <h1 className="permission-error">You don't have permission to see this section. Contact to administrator.</h1>
            </div>
        }
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-6">
                        <h4>Edit User</h4>
                        { this.state.success == "1"  && <p className="alert alert-success">{this.state.message}</p>}
                        { this.state.success == "0" && <p className="alert alert-danger">{this.state.message}</p>}
                    </div>
                </div>
                <form onSubmit={this.handleSubmit}>
                <div className="row">
                    <div className="col-md-6">
                        <div className="form-group">
                            <label>User Role</label>
                            <select name="role_id" className="form-control" onChange={this.handleChange} value={this.state.role_id}>
                                <option value="1">Administrator</option>
                                <option value="2">User</option>
                            </select>
                        </div>
                        <div className="form-group">
                            <label>Name</label>
                            <input
                                name="name"
                                placeholder="Name"
                                value={this.state.name}
                                onChange={this.handleChange}
                                className="form-control"
                            />
                            <div className="error">
                                {this.state.nameError}
                            </div>
                        </div>
                        <div className="form-group">
                            <label>Email</label>
                            <input
                                name="email"
                                placeholder="Email"
                                value={this.state.email}
                                onChange={this.handleChange}
                                className="form-control"
                            />
                            <div className="error">
                                {this.state.emailError}
                            </div>
                        </div>
                        <div className="form-group">
                            <label>Status</label>
                            <select name="status" className="form-control" onChange={this.handleChange} value={this.state.status}>
                                <option value="1">Active</option>
                                <option value="0">Deactive</option>
                            </select>
                        </div>
                        <input type="hidden" name="user_id" value={this.state.user_id} onChange={this.handleChange}/>
                        <button type="submit" className="btn btn-warning">
                            Submit <i className="fa fa-check-circle" aria-hidden="true"></i>
                        </button>
                        <a href="/users" className="btn btn-warning back-to-list">
                           Cancel <i className="fa fa-sign-in" aria-hidden="true"></i>
                        </a>
                        <img src={loader} className="loader-user" id="loader-user" style={{display:"none"}}/>
                    </div>
                </div>
                </form>
            </div>
        );
    }
}

export default EditUser;