import React, {Component} from 'react';
import {Redirect} from "react-router-dom";
class Listing extends Component {
    constructor() {
        super();
        this.state = {
            editUser:""
        }
        this.editUser = this.editUser.bind(this);
    }
    editUser(){
       console.log(this.props);
        this.setState({editUser: this.props});
    }
    render() {
        if (this.state.editUser) {
            return (<Redirect to={'/editUser/'+this.props.user_id}/>)
        }
        return <React.Fragment>
            <tr>
                <td>{this.props.name}</td>
                <td>{this.props.email}</td>
                <td>{this.props.role_name}</td>
                <td><span className={(this.props.status == 'Active')? 'badge badge-success':'badge badge-danger'}>{this.props.status}</span></td>
                <td>
                    <a  href="javascript:void(0);"  className="btn btn-warning" onClick={this.editUser.bind(this.props)}>
                        <i className="fa fa-pencil"></i>&nbsp;
                        Edit
                    </a>&nbsp;
                    <a  href="javascript:void(0);"  className="btn btn-danger">
                        <i className="fa fa-trash"></i>&nbsp;
                        Delete
                    </a>
                </td>
            </tr>
        </React.Fragment>
    }
}

export default Listing;