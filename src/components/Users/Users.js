import React, {Component} from 'react';
import {checkLogin,checkPermission} from '../../Utils';
import {Redirect} from "react-router-dom";
import {UserListing} from '../../services/UserListing';
import Listing from "../Users/Listing";
class Users extends Component {
    constructor() {
        super();
        this.state = {
            userList:"",
            tableColumns:""
        }
    }

    async componentDidMount() {
        UserListing('userListing', this.state).then((result) => {
            let responseJson = result;
            if (responseJson.results) {
                var obj=responseJson.results;
                if(obj.statusCode=="1") {
                    this.setState({userList: responseJson.results.result});
                }else{
                    this.setState({userList: ""});
                }
            }
        });
    }
    render() {
        if(checkLogin()==false){
            return (<Redirect to={'/login'}/>)
        }
        if(checkPermission()==false){
            return <div className="text-center">
                <h1 className="permission-error">You don't have permission to see this section. Contact to administrator.</h1>
            </div>
        }else{
            if (this.state.userList) {
                var userListing = [];
                userListing = this.state.userList.map((listing, i) => {
                    return <Listing key = { this.state.userList[i].user_id }
                                    user_id={this.state.userList[i].user_id}
                                    name = { this.state.userList[i].name }
                                    email = { this.state.userList[i].email }
                                    role_name = { this.state.userList[i].role_name }
                                    status = { this.state.userList[i].status }
                    />
                });
            }
            return (
                <div>
                    <div className = "container" >
                        <h4 className="user-listing-heading">User Listing <i className="fa fa-users" aria-hidden="true"></i></h4>
                        <table border = "1" className = "w-100" >
                            <thead>
                            <th> Name </th>
                            <th> E-mail </th>
                            <th> User Role </th>
                            <th> Status </th>
                            <th> Action  </th>
                            </thead>
                            <tbody> { userListing } </tbody>
                        </table>
                    </div>
                </div>
            )
        }
    }
}

export default Users;