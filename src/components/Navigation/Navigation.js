import React, {Component} from 'react';
import {Route, Link, BrowserRouter as Router, Redirect} from 'react-router-dom';
class Navigation extends Component {
    constructor(){
        super();

    }

    render() {
            return (

                <div>
                    <ul className="nav_links">
                        <li>
                            <Link to="/">Home</Link>
                        </li>
                        <li>
                            <Link to="/Users">Users</Link>
                        </li>
                        <li>
                            <Link to="/Avatar">Avatar</Link>
                        </li>
                        <li>
                            <Link to="/ImageSlider">ImageSlider</Link>
                        </li>
                        <li>
                            <Link to="/ProductListing">ShoppingCart</Link>
                        </li>
                        <li>
                            <Link to="/login">Login</Link>
                        </li>
                    </ul>
                </div>
            );

    }
}

export default Navigation;