import React from "react";
import {CreateUser} from '../../services/CreateUser';
import {Redirect} from "react-router-dom";
import loader from "../../img/loader.gif";
const initialState = {
  name: "",
  email: "",
  password: "",
  nameError: "",
  emailError: "",
  passwordError: "",
  registrationMessage:"",
  registrationSuccess:false,
  registrationError:false,
  redirectToReferrer: false
};

export default class Registration extends React.Component {
  state = initialState;

  handleChange = event => {
    const isCheckbox = event.target.type === "checkbox";
    this.setState({
      [event.target.name]: isCheckbox
        ? event.target.checked
        : event.target.value
    });
  };

  validate = () => {
    let nameError = "";
    let emailError = "";
    let passwordError = "";

    if (!this.state.name) {
      nameError = "name cannot be blank";
    }

    if (!this.state.email.includes("@")) {
      emailError = "invalid email";
    }

    if (!this.state.passwordError) {
      passwordError = "Password cannot be blank";
    }

    if (emailError || nameError || passwordError) {
      this.setState({ emailError, nameError,passwordError });
      return false;
    }

    return true;
  };
  login(){
    this.setState({redirectToReferrer: true});
  }
  handleSubmit = event => {
    event.preventDefault();
    const isValid = this.validate();
    if (isValid) {
      let loader=document.getElementById("loader-reg");
      loader.style.display="block";
      CreateUser('registration', this.state).then((result) => {
        let responseJson = result;
        if (responseJson.userData) {
          let obj=responseJson.userData[0];
          loader.style.display="none";
          if(obj.statusCode=="0"){
            this.setState({registrationError: true,registrationMessage:obj.message,emailError:"",nameError:"",passwordError:""});
          }else{
            this.setState(initialState);
            this.setState({registrationSuccess: true,registrationMessage:obj.message});
          }
        }
      });
    }
  };

  render() {
    if (this.state.redirectToReferrer) {
      return (<Redirect to={'/'}/>)
    }

    return (
        <form onSubmit={this.handleSubmit}>
          <div className="container">
            <div className="row">
              <div className="col-md-6">
                <h4>Registration</h4>
                { this.state.registrationSuccess && <p className="alert alert-success">{this.state.registrationMessage}</p>}
                { this.state.registrationError && <p className="alert alert-danger">{this.state.registrationMessage}</p>}
              </div>
            </div>
            <div className="row">
              <div className="col-md-6">
                <div className="form-group">
                  <label>Name</label>
                  <input
                      name="name"
                      placeholder="Name"
                      value={this.state.name}
                      onChange={this.handleChange}
                      className="form-control"
                  />
                  <div className="error">
                    {this.state.nameError}
                  </div>
                </div>
                <div className="form-group">
                  <label>Email</label>
                  <input
                      name="email"
                      placeholder="Email"
                      value={this.state.email}
                      onChange={this.handleChange}
                      className="form-control"
                  />
                  <div className="error">
                    {this.state.emailError}
                  </div>
                </div>
                <div className="form-group">
                  <label>Password</label>
                  <input
                      type="password"
                      name="password"
                      placeholder="Password"
                      value={this.state.password}
                      onChange={this.handleChange}
                      className="form-control"
                  />
                  <div className="error">
                    {this.state.passwordError}
                  </div>
                </div>
                <button type="submit" className="btn btn-warning">
                  Submit <i className="fa fa-check-circle" aria-hidden="true"></i>
                </button>
                <button type="button" className="btn btn-warning already-login-btn" onClick={this.login.bind(this)}>
                  Already a member ? Login here <i className="fa fa-sign-in" aria-hidden="true"></i>
                </button>
                <img src={loader} className="loader-reg" id="loader-reg" style={{display:"none"}}/>
              </div>
            </div>
          </div>
        </form>
    );
  }
}
