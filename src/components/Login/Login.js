import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';
import {UserLogin} from '../../services/UserLogin';
import loader from "../../img/loader.gif";
class Login extends Component {
  constructor(){
    super();
    this.state = {
        email: "",
        password: "",
        emailError: "",
        passwordError: "",
        invalidCredential:"0",
        redirectToReferrer: false,
        registration:false,
        loginMessage:"",
        forgotPassword:false
    };
    this.login = this.login.bind(this);
    this.registration = this.registration.bind(this);
    this.forgotPassword= this.forgotPassword.bind(this);
    this.onChange = this.onChange.bind(this);
  }

    validate = () => {
        let passwordError = "";
        let emailError = "";
        if (!this.state.passwordError) {
            passwordError = "Password cannot be blank";
            this.setState({passwordError:passwordError});
            return false;
        }
        if (!this.state.email.includes("@")) {
            emailError = "Invalid email";
            this.setState({ emailError:emailError});
            return false;
        }
        this.setState({ emailError:"", passwordError:""});
        return true;
    };

  login() {
          const isValid = this.validate();
          if (this.state.email && this.state.password) {
              let loader=document.getElementById("loader-login");
              loader.style.display="block";
              UserLogin('login', this.state).then((result) => {
                  let responseJson = result;
                  if (responseJson.userData) {
                      loader.style.display="none";
                      let obj=responseJson.userData[0];
                      if(obj.statusCode=="1"){
                          sessionStorage.setItem('userData', JSON.stringify(obj.result));
                          this.setState({invalidCredential: "1",loginMessage:obj.message,emailError:"",passwordError:""});
                      }else{
                          this.setState({invalidCredential: "1",loginMessage:obj.message,emailError:"",passwordError:""});
                      }

                  }

              });
          }
   }

   registration(){
       this.setState({registration: true});
   }

   forgotPassword(){
       this.setState({forgotPassword: true});
   }

  onChange(e){
    this.setState({[e.target.name]:e.target.value});
   }

  
  

  render() {

     if (this.state.redirectToReferrer) {
      return (<Redirect to={'/'}/>)
    }

    if (this.state.registration) {
        return (<Redirect to={'/Registration'}/>)
    }

    if (this.state.forgotPassword) {
        return (<Redirect to={'/forgotPassword'}/>)
    }
   
    if(sessionStorage.getItem('userData')){
      return (<Redirect to={'/'}/>)
    }

     return (
         <div>
             <div className = "container" >
                  <div className="row">
                        <div className="col-md-6">
                            <h4>Login</h4>
                            { this.state.invalidCredential == "1" && <p className="alert alert-danger">{this.state.loginMessage}</p>}
                        </div>
                  </div>
                 <div className="row">
                        <div className="col-md-6">
                            <div className="form-group">
                                <label>E-mail</label>
                                <input type="text" name="email" placeholder="E-mail" className="form-control" onChange={this.onChange}/>
                                <div className="error">
                                    {this.state.emailError}
                                </div>
                            </div>
                        </div>
                 </div>
                 <div className="row">
                        <div className="col-md-6">
                            <div className="form-group">
                                <label>Password</label>
                                <input type="password" name="password"  placeholder="Password" className="form-control" onChange={this.onChange}/>
                                <div className="error">
                                    {this.state.passwordError}
                                </div>
                            </div>
                        </div>
                  </div>
                  <div className="row">
                      <div className="col-md-6">
                          <div className="form-group">
                              <button type="submit" className="btn btn-warning success" onClick={this.login}>
                                  Login <i className="fa fa-sign-in" aria-hidden="true"></i>
                              </button>

                              <button type="button" className="btn btn-warning registration-btn" onClick={this.registration}>
                                  Registration <i className="fa fa-user-plus" aria-hidden="true"></i>
                              </button>
                              <img src={loader} className="loader-login" id="loader-login" style={{display:"none"}}/>
                          </div>
                      </div>
                  </div>
                  <div className="row">
                      <div className="col-md-6">
                          <div className="form-group">
                             <a href="javascript:void(0)" onClick={this.forgotPassword}>
                                 <i className="fa fa-key" aria-hidden="true"></i>&nbsp;
                                 Forgot password ?</a>
                          </div>
                      </div>
                  </div>
             </div>
         </div>
    );
  }
}

export default Login;