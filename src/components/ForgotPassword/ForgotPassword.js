import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';
import {GetPassword} from '../../services/GetPassword';
import loader from "../../img/loader.gif";
class ForgotPassword extends Component {
    constructor(){
        super();
        this.state = {
            email: "",
            emailError: "",
            error:"",
            success:"",
            messages:""
        };
        this.forgotPassword= this.forgotPassword.bind(this);
        this.onChange = this.onChange.bind(this);
    }
    onChange(e){
        this.setState({[e.target.name]:e.target.value});
    }
    validate = () => {
        let emailError = "";
        if (!this.state.email.includes("@")) {
            emailError = "Invalid email";
        }
        if (emailError) {
            this.setState({ emailError:emailError});
            return false;
        }
        this.setState({ emailError:""});
        return true;
    };
    forgotPassword(){
        const isValid = this.validate();
        if (this.state.email) {
            let loader=document.getElementById("process-loader");
            loader.style.display="block";
            GetPassword('forgotpassword', this.state).then((result) => {
                let responseJson = result;
                if (responseJson.userData) {
                    let obj=responseJson.userData[0];
                    loader.style.display="none";
                    if(obj.statusCode=="0"){
                        this.setState({error: "1",success:"",messages:obj.message,emailError:""});
                    }else{
                        this.setState({success: "1",error:"",messages:obj.message,emailError:"",email:""});
                    }
                }

            });
        }
    }
    render() {
        return (
            <div>
                <div className = "container" >
                    <div className="row">
                        <div className="col-md-6">
                            <h4>Forgot Password</h4>
                            { this.state.success && <p className="alert alert-success">{this.state.messages}</p>}
                            { this.state.error && <p className="alert alert-danger">{this.state.messages}</p>}
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6">
                            <div className="form-group">
                                <label>E-mail</label>
                                <input type="text" name="email" placeholder="E-mail" className="form-control" onChange={this.onChange}/>
                                <img src={loader} className="process-loader" style={{display:"none"}} id="process-loader"/>
                                <div className="error">
                                    {this.state.emailError}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6">
                            <div className="form-group">
                                <button type="submit" className="btn btn-warning" onClick={this.forgotPassword}>
                                    Forgot Password <i className="fa fa-check-circle" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ForgotPassword;