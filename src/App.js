import React, { Component } from "react";
import "./App.css";
import FetchRandomUser from "./components/RandomUser/FetchRandomUser";
import {Redirect} from "react-router-dom";
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      redirectToReferrer: false,
      name:'',
      visible: true
    };
    this.logout = this.logout.bind(this);
  }
  componentWillMount() {
    var userData=sessionStorage.getItem("userData");
    if(userData){
      let result=JSON.parse(userData);
      this.setState({name: result.userData[0]["name"]});
    }else{
      this.setState({redirectToReferrer: true});
    }
  }
  logout(){
    sessionStorage.setItem("userData",'');
    sessionStorage.clear();
    this.setState({redirectToReferrer: true});
  }
  render() {
    if (this.state.redirectToReferrer) {
      return (<Redirect to={'/login'}/>)
    }
    return (
      <div className="App">
        <h4>Welcome: { this.state.name}</h4>
        <a href="javascript:void(0);" onClick={this.logout} className="btn btn-warning logout">Logout <i
            className="fa fa-sign-out" aria-hidden="true"></i>
        </a>
        <FetchRandomUser />
      </div>
    );
  }
}
export default App;
