export const checkLogin = () => {
    var userData=sessionStorage.getItem("userData");
    if(userData){
       return true;
    }else{
        return false;
    }
};

export const checkRole = () => {
    var userData=sessionStorage.getItem("userData");
    if(userData){
       let result=JSON.parse(userData);
       if(result.userData[0]["role_id"]=="1"){
         return true;
       }else{
       	 return false;
       }
    }else{
        return false;
    }
};

export const checkPermission = () => {
    let userData=sessionStorage.getItem("userData");
    if(userData == null){
      return false;
    }
    let result=JSON.parse(userData);
    let user_permission=result.userData[0]["permissions"];
    if(result.userData[0]["role_id"]=="1"){
       return true;
    }else{
        if(user_permission == ""){
            return false;
        }else {
            let section = window.location.pathname;
            section = section.slice(1);
            var permission = user_permission.indexOf(section);
            if (permission >= 0) {
                return true;
            } else {
                return false;
            }
        }
    }
};
