import React from 'react';
import ReactDOM from 'react-dom';
import { Route, Link, BrowserRouter as Router } from 'react-router-dom';
import './index.css';
import Avatar from './components/Avatar/Avatar';
import Registration from './components/Registration/Registration';
import ImageSlider from './components/ImageSlider/ImageSlider';
import ShoppingCart from './components/ShoppingCart/ShoppingCart';
import ProductListing from './components/ShoppingCart/ProductListing';
import Login from './components/Login/Login';
import ForgotPassword from './components/ForgotPassword/ForgotPassword';
import Users from './components/Users/Users';
import editUser from './components/Users/editUser';
import Permissions from './components/Permissions/Permissions';
import Navigation from './components/Navigation/Navigation';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
const routing = (  
  <Router>  
    <div>  
      <h1>React</h1>  
      <Navigation/>
      <div className="content">  
      <Route exact path="/" component={App} />  
      <Route path="/Avatar" component={Avatar} /> 
      <Route path="/Registration" component={Registration} />
      <Route path="/ImageSlider" component={ImageSlider} />
      <Route path="/ShoppingCart" component={ShoppingCart} />
      <Route path="/ProductListing" component={ProductListing} />
      <Route path="/login" component={Login} />
      <Route path="/forgotPassword" component={ForgotPassword} />
      <Route path="/Users" component={Users} />
      <Route path="/editUser/:user_id" component={editUser} />
      <Route path="/Permissions" component={Permissions} />
      </div>   
    </div>  
  </Router>  
)
ReactDOM.render(routing, document.getElementById('root'));
registerServiceWorker();
